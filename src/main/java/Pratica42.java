
import utfpr.ct.dainf.if62c.pratica.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gabriel
 */
public class Pratica42 {
    public static void main(String[] args) {
        Elipse e = new Elipse(1.5, 2.5);
        Circulo c = new Circulo(2.5);
        System.out.println("Perímetro da Elipse:" + e.getPerimetro());
        System.out.println("Área da Elipse:" + e.getArea());
        System.out.println("Eixo Maior da Elipse:" + e.getEixoMaior());
        System.out.println("Eixo Menor da Elipse:" + e.getEixoMenor());
        System.out.println("Perímetro do Círculo:" + c.getPerimetro());
    }
}
