/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author gabriel
 */
public class Elipse implements FiguraComEixos {
    private double r, s;
    
    public Elipse(double semi_eixo_x, double semi_eixo_y){
        r = semi_eixo_x;
        s = semi_eixo_y;
    }
    
    @Override
    public double getArea(){
        return Math.PI * r * s;

    }
    
    @Override
    public double getPerimetro(){
       return Math.PI * (3 * (r + s) - Math.sqrt((3 * r + s)*(r + 3 * s)));
    }
    @Override
    public String getNome(){
        return this.getClass().getSimpleName();
    }
    
    @Override
    public double getEixoMenor(){
        return (r<s)?r:s;
    }
            
    @Override
    public double getEixoMaior(){
        return (r>s)?r:s;
    }
}
